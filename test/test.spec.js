const chai = require("chai");
const chaihttp = require("chai-http");
const url = require('../src/server');

chai.use(chaihttp);

const assert = chai.assert;

describe("Simple test", ()=>{
    it("Teste", done => {
        chai
            .request(url)
            .get("/")
            .end((err, res) => {
                chai.expect(res).to.have.status(201);
                done();
            });
    });
});