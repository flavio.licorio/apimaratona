const jwt = require('jsonwebtoken');

exports.optional = (req, res, next) => {
    res.locals.id_usuario = 0;
    try{
        const token = req.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, "querino");
        req.userData = decoded;
        res.locals.email = req.userData.email;
        next();
    } catch(error){
        next()
    }
}

exports.required = (req, res, next) => {
    res.locals.email = 0;
    try{
        const token = req.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, "querino");
        req.userData = decoded;
        res.locals.email = req.userData.email;
        next();
    } catch(error){
        return res.status(401).send({
            massage : "Usuario não autenticado"
        });
    }
}
