const express = require('express');
const router = express.Router();
const purchaseController = require('../controllers/purchase-controller');


router.get('/', purchaseController.getPurchases);

router.get('/:id', purchaseController.getPurchase);

// router.delete('/', purchaseController.deletePurchase);

router.post('/', purchaseController.postPurchase);

// router.put('/', purchaseController.putPurchase);

module.exports = router;