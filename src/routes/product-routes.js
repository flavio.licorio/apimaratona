const express = require('express');
const router = express.Router();
const productController = require('../controllers/product-controller');
const login = require('../middleware/login-middleware')


router.get('/', productController.getProducts);

router.get('/:id', productController.getProduct);

router.delete('/', productController.deleteProduct);

router.post('/', productController.postProduct);

router.put('/', productController.putProduct);

module.exports = router;