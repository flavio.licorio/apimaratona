const express = require('express');
const router = express.Router();
const userController = require('../controllers/user-controller');

router.get('/', userController.getUsers);

router.delete('/', userController.deleteUser);

router.post('/', userController.postUser);

router.post('/login', userController.loginUser);

module.exports = router;