const pool = require('../mysql').pool;

exports.getProducts = (req, res, next) => {
    pool.getConnection(
        (err, conn) => {
            if (err) {
                return res.status(406).json({
                    'error': err
                });
            } else {
                conn.query(`select * from produtos;`, (err, values) => {
                    if (err) {
                        return res.status(406).json({
                            'error': err
                        })
                    } else {
                        return res.status(200).json(values);
                    }
                });
            }
        }
    );
}

exports.getProduct = (req, res, next) => {
    pool.getConnection(
        (err, conn) => {
            if (err) {
                return res.status(406).json({
                    'error': err
                });
            } else {
                conn.query(`select * from produtos where idprodutos = ${req.params.id};`, (err, values) => {
                    if (err) {
                        return res.status(406).json({
                            'error': err
                        })
                    } else {
                        return res.status(200).json(values);
                    }
                });
            }
        }
    );
}

exports.postProduct = (req, res, next) => {
    pool.getConnection((err, conn) => {
        if (err) {
            return res.status(406).json({
                "error": err
            });
        } else {
            conn.query(
                `insert into produtos (
                    nomeProduto,
                    descricaoProduto,
                    precoProduto
                ) values (
                    ?,?,?
                )`, [req.body.nomeProduto, req.body.descricaoProduto, req.body.precoProduto],
                (err, values) => {
                    if (err) {
                        return res.status(406).json({
                            'error': err
                        });
                    } else {
                        return res.status(201).json({
                            'response': 'success'
                        });
                    }
                });
        }
    });
}

exports.deleteProduct = (req, res, next) => {
    pool.getConnection(
        (err, conn) => {
            if (err) {
                return res.status(406).json({
                    'error': err
                });
            } else {
                conn.query(`delete from produtos where idprodutos = ${req.body.id}`, (err, values) => {
                    if (err) {
                        return res.status(406).json({
                            'error': err
                        })
                    } else {
                        return res.status(200).json(values);
                    }
                });
            }
        }
    );
}
exports.putProduct = (req, res, next) => {
    pool.getConnection(
        (err, conn) => {
            if (err) {
                return res.status(406).json({
                    'error': err
                });
            } else {
                conn.query(
                    `update produtos set nomeProduto = ?, descricaoProduto = ?, precoProduto = ? where idprodutos = ?`,
                    [
                        req.body.nomeProduto,
                        req.body.descricaoProduto,
                        req.body.precoProduto,
                        req.body.id
                    ],
                    (err, values) => {
                        if (err) {
                            return res.status(406).json({
                                'error': err
                            });
                        } else {
                            return res.status(201).json({
                                'response': 'success'
                            });
                        }
                    });
            }
        }
    )
}


