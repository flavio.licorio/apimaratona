const pool = require('../mysql').pool;
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

exports.getUsers = (req, res, next) => {
    pool.getConnection(
        (err, conn) => {
            if (err) {
                return res.status(406).json({
                    'error': err
                });
            }
            conn.query(`select * from usuarios;`, (err, values) => {
                if (err) {
                    return res.status(406).json({
                        'error': err
                    })
                } else {
                    return res.status(200).json(values);
                }
            });

        }
    );
}

exports.deleteUser = (req, res, next) => {
    pool.getConnection(
        (err, conn) => {
            if (err) {
                return res.status(406).json({
                    'error': err
                });
            }
            conn.query(`delete from usuarios where idusuarios = ${req.body.id}`, (err, values) => {
                if (err) {
                    return res.status(406).json({
                        'error': err
                    })
                }
                return res.status(200).json(values);

            });

        }
    );
}

exports.postUser = (req, res, next) => {
    pool.getConnection((err, conn) => {
        if (err) { return res.status(500).send({ error: err }); }
        conn.query('select 1 from usuarios where email = ?', [req.body.email], (err, results) => {
            if (err) {
                return res.status(500).send({ error: err });
            } else if (results.length > 0) {
                return res.status(500).send({ error: "Usuario já cadastrado" });
            }
            bcrypt.hash(req.body.password, 10, (errBcrypt, hash) => {
                if (err) {
                    return res.status(500).send({ error: errBcrypt });
                }
                conn.query(
                    'insert into usuarios (nome, email, senha)'
                    + 'values(?,?,?)',
                    [
                        req.body.name,
                        req.body.email,
                        hash
                    ],
                    (error, result, fields) => {
                        conn.release();
                        if (error) {
                            return res.status(500).send({
                                error: error
                            });
                        }
                        let token = jwt.sign({
                            nome: req.body.nome,
                            email: req.body.email
                        }, "querino", {});
                        return res.status(201).send({
                            message: "Sucesso",
                            id_usuario: result.insertId,
                            token: token
                        });
                    }
                );
            });
        });
    });
}

exports.loginUser = (req, res, next) => {
    pool.getConnection((err, conn) => {
        if (err) { return res.status(500).send({ error: err }) }
        conn.query(`select * from usuarios where email = ?`, [req.body.email], (error, results) => {
            conn.release();
            if (error) { return res.status(500).send({ error: error }) }
            if (results.length < 1) { return res.status(401).send({ error: "Usuario não existe" }) }
            bcrypt.compare(req.body.password, results[0].senha, (errBcrypt, result) => {
                if (errBcrypt) { return res.status(401).send({ error: "Erro senha" }) }
                if (result) {
                    let token = jwt.sign({
                        name: results[0].nome,
                        email: results[0].email
                    }, "querino", {});
                    return res.status(200).send({
                        messagem: "Sucesso",
                        name: results[0].nome,
                        email: results[0].email,
                        token: token
                    })
                }
                return res.status(401).send({ error: "Falha na autenticação" })
            });
        });
    });
}

