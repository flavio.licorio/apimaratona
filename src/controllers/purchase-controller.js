const pool = require('../mysql').pool;

exports.getPurchases = (req, res, next) => {
    pool.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }); }
        conn.query(
            'select * from pedidos', (error, result, fields) => {
                conn.release();
                if (error) { res.status(500).send({ error: error }) }
                else { res.status(200).send({ message: "Sucesso", purchases: result }) }
            }
        );
    });
}

exports.getPurchase = (req, res, next) => {
    pool.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }); }
        conn.query(
            'select * from pedidos where idpedidos = ?', [req.params.id], (error, result, fields) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }); }
                return res.status(200).send({ message: "Sucesso", purchase: result });
            }
        );
    });
}

exports.postPurchase = (req, res, next) => {
    pool.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }); }
        console.log(Date.now());
        // conn.query(
        //     `insert into pedidos(datahora, usuarios_idusuarios) values(?,?)`, [Date.now(), req.body.userId],
        //     (error, result, fields) => {
        // if (error) { return res.status(500).send({ error: "first query error: " + error }); }
        for (product in req.body.products) {
            console.log(product)
            conn.query(
                `insert into pedidos_has_produtos(pedidos_idpedidos, produtos_idprodutos, qtd_produto)
                        values(?,?,?)`,
                [result.insertId, product.productId, product.quantity], (error, result, fields) => {
                    console.log(product);
                    if (error) { res.status(500).send({ error: "second query error: " + error }); }
                }
            );
        }
        res.status(201).send({ message: "Success" });
        // }
        // )
    })
}
