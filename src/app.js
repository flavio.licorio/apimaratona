const express = require('express');
const app = express();
const morgan = require('morgan');
var cors = require('cors');

const routeProduct = require('./routes/product-routes');
const routePurchase = require('./routes/purchase-routes');
const routeUser = require('./routes/user-routes');

app.use(morgan('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(cors());


app.use('/products', routeProduct);
app.use('/purchases', routePurchase);
app.use('/users', routeUser);


app.use((req, res, next) => {
    const erro = new Error('Não encontrado');
    erro.status = 404;
    next(erro);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        mensagem: error.message
    });
});

module.exports = app;
